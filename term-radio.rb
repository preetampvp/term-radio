#!/usr/bin/env ruby
require 'json'

file = File.read("./stations.json")
data = JSON.parse(file)
stations = data["stations"]

def playstation(url, isPlaylist=false)
  if isPlaylist == true
    system "mplayer -playlist #{url}"
  else
    system "mplayer #{url}"
  end
end

puts "checking for mplayer"
check = system "which mplayer"
if check == false
  puts "!! mplayer required - install using your fav system package manager !!"
  exit 1
end

trap("INT") {
  puts "\nThank you for listening."
  exit 0
}

if ARGV.size == 0
  counter = 0
  puts "\n\n** Stations **"
  stations.each do |station|
    counter=counter+1
    print("#{counter}. #{station["name"]}")
    if counter % 3 == 0
      print "\n"
    else
      print "\t\t\t\t"
    end
  end
  print "\n\nPick a station # to play: "
  stationNo = gets
  stationNo = stationNo.to_i - 1

  station = stations[stationNo]
  playstation(station["url"], station["isPlaylist"])
else
  station = stations[(ARGV[0].to_i - 1)]
  playstation(station["url"], station["isPlaylist"])
end
